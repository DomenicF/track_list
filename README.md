# Track List

[![Gem Version](https://badge.fury.io/rb/track_list.svg)](https://badge.fury.io/rb/track_list)

This program aims to provide an easy way to get a track list for an album in your terminal, or you could use it in your own program if you wish. Define your own custom template to output your track lists or use the predefined one. The predefined one is below. If you'd like to customize this output, scroll down to Configuration.

## Sample Output
```
1. Tellin' Lies (0:03:59)
2. Thick as Thieves (0:03:15)
3. Lookers (0:03:37)
4. Midwestern States (0:03:27)
5. Charlie's Army (0:02:31)
6. House on Fire (0:03:37)
7. Black Mass (0:02:52)
8. Boy Blue (0:03:20)
9. Bad Catholics (0:02:52)
10. Your Wild Years (0:03:54)
11. The Bars (0:04:11)
12. After the Party (0:03:50)
13. Livin' Ain't Easy (0:03:26)
```

## Installation

First, install [TagLib](https://taglib.org/). 

Homebrew: `brew install taglib`

Ubuntu/Debian: `sudo apt install libtag1-dev`

Arch: `sudo pacman -S taglib`

Add this line to your application's Gemfile:

```ruby
gem 'track_list'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install track_list

## Configuration

You can define custom output for your track list. On first run of the program, the program will create a `settings.yaml` file in the `$HOME/.config/track_list` directory.

If you would like to change this location, set the environment variable `TRACKLIST_CONFIG` to where you'd like the file.

The default output is as follows:

`output: "%TRACK%. %TITLE% (%LENGTH%)"`

You can change this string with the following tags to format it the way you want it:
- `%TRACK%` - The track number of a given audio file.
- `%TITLE%` - The title tag of a given audio file.
- `%LENGTH%` - The length of a given audio file in `[hours:minutes:seconds]`
- `%LENGTH_IN_MILLISECONDS%` - The length of a given audio file in milliseconds.
- `%ARTIST%` - The artist tag of a given audio file.
- `%ALBUM%` - The album tag of a given audio file.
- `%YEAR%` - The year tag of a given audio file.
- `%GENRE%` - The genre tag of a given audio file.
- `%COMMENT%` - The comment tag of a given audio file.

If a tag does not exist, you should see an empty space in the output. If you are getting some sort of error, please file an issue.

## Usage

`tracklist <album directory>`


## Contributing

Bug reports and pull requests are welcome on GitLab at https://gitlab.com/DomenicF/track_list/issues.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
