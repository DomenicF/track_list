# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.3.0] - 2022-07-28
- Added option for `%LENGTH_IN_MILLISECONDS%` in output.
- Changed YAML file location to `$HOME/.config/track_list/settings.yaml`.
- Added a way to change the above location with environment variable `TRACKLIST_CONFIG`.
- Bumped dependency versions.
- General code cleanup.

## [0.2.0] - 2019-08-09
- Added custom templating so a user can define what is output to their tracklist.
- TemplateParser class that helps with reading/writing to a config/template file.
- Commented code with RDoc.
- Cleaned up codebase more.

## [0.1.1] - 2019-08-08
### Fixed
- Missing ARGV argument no longer throws a fatal error but shows a helpful message instead.
- Cleaned up codebase a bit.

## [0.1.0] - 2019-08-08
### Added
- Separation of concerns compared to old codebase. Current codebase is cleaner.
- TrackParser class that parses a single track.
- DirectoryParser class that parses a directory, using TrackParser.
- TimeConverter class that puts seconds into hour:minute:seconds for easy readability on track lengths.
