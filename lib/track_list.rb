require 'track_list/version'
require 'track_list/track_parser'
require 'track_list/directory_parser'
require 'track_list/time_converter'
require 'track_list/template_parser'
require 'yaml'

# This module defines the base of our program.

module TrackList
  class Error < StandardError; end
end
