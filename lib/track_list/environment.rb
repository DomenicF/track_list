module TrackList
  class Environment
    def initialize
      if ENV.has_value?("TRACKLIST_CONFIG")
        @config_path = ENV["TRACKLIST_CONFIG"]
      else
        @config_path = Dir.home() + '/.config/track_list/settings.yaml'
      end
    end

    def get_config_path
      @config_path
    end
  end
end