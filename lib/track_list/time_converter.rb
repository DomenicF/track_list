module TrackList

    # A helper class to convert an audio track's length in seconds to a more readable format.

    class TimeConverter

        # Pass in an integer of seconds to this class.

        def initialize(time)
            @time = time
        end

        # This method formats the time into a readable format.
        # See: https://www.codethought.com/2010/01/seconds-minutes-hours-converting-time-units-in-ruby/

        def format_time
            # Find the seconds.
            seconds = @time % 60
        
            # Find the minutes.
            minutes = (@time / 60) % 60
        
            # Find the hours.
            hours = (@time / 3600)
        
            # Format the time.
            return hours.to_s + ":" + format("%02d", minutes.to_s) + ":" + format("%02d", seconds.to_s)
        end
    end
end