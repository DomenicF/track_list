require 'track_list/track_parser'

module TrackList

    # This class is a helper to parse through an entire directory of tracks
    # and return an array of parsed, formatted tracks.

    class DirectoryParser

        # When initializing the class, pass in a directory with audio files
        # to be parsed.
        #
        # @param [String] dir The directory to be parsed.

        def initialize(dir)
            @dir = dir
        end

        # This method gets a list of files to be parsed.
        #
        # @return [Array] Files to be parsed.

        def list
            files = []
            Dir.foreach(@dir) do |file|
                files.push(@dir + '/' + file)
            end
            return files
        end

        # This method does the bulk of the work. It loops over each of the files we
        # got in +self.list+ and returns an array of +TrackList::TrackParser+ objects
        # to be looped over elsewhere.
        #
        # @return [Array] of +TrackList::TrackParser+ objects.

        def parse
            files = self.list.sort
            parsed_tracks = []
            files.each do |filename, index|
                track = TrackList::TrackParser.new(filename)
                if track != nil || track != ''
                    parsed_tracks.push(track)
                end
            end
            return parsed_tracks
        end

        
    end
end
