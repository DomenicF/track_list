require 'taglib'
require 'track_list/environment'
require 'track_list/time_converter'
require 'track_list/template_parser'

module TrackList
  ##
  # The purpose of this class is to parse a single track and return a formatted
  # string.

  class TrackParser

    ##
    # When initializing the class, you must pass in a file path to the +track+
    # you want parsed. 
    #
    # @param [String] track The track to be parsed.
    
    def initialize(track)
        @track = track
    end

    ##
    # The bulk of the work is done in this method. Calling this method returns
    # a parsed track in a formatted string. 
    #
    # @return [String] if valid audio file.
    
    def parse
        TagLib::FileRef.open(@track) do |fileref|
          unless fileref.null?
            tag = fileref.tag
            properties = fileref.audio_properties
            time_conversion = TrackList::TimeConverter.new(properties.length_in_seconds)
            length = time_conversion.format_time

            template_parser = TrackList::TemplateParser.new
            template = template_parser.load

            template_strings = {
              '%TRACK%' => tag.track.to_s,
              '%TITLE%' => tag.title,
              '%LENGTH%' => length,
              '%LENGTH_IN_MILLISECONDS%' => properties.length_in_milliseconds.to_s,
              '%ARTIST%' => tag.artist,
              '%ALBUM%' => tag.album,
              '%YEAR%' => tag.year.to_s,
              '%GENRE%' => tag.genre,
              '%COMMENT%' => tag.comment 
            }

            parsed_string = ''

            template_strings.each do |key, val|
              if template['output'].include? key
                parsed_string = template['output'].gsub!(key, val)
              end
            end

            return parsed_string
        end
      end
    end

  end
end