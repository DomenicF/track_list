require 'fileutils'
require 'yaml'
require 'track_list/environment'

module TrackList

    # A helper class to read/write a config file.

    class TemplateParser

        # Pass in a file path to read/write the config file.

        def initialize
            environment = Environment.new
            @config_path = environment.get_config_path
        end

        # Return the config file in a Ruby-readable format.

        def load
            YAML.load(File.open(File.expand_path(@config_path)))
        end

        # Check if the config file already exists.

        def config_exists?
            if File.exist?(File.expand_path(@config_path))
                true
            else
                false
            end
        end

        # Create a default config file with some basic settings.

        def create_default_config
            config = { "output" => "%TRACK%. %TITLE% (%LENGTH%)" }
            FileUtils.mkdir_p File.dirname(@config_path)
            out_file = File.new(File.expand_path(@config_path), 'w')
            out_file.puts(config.to_yaml)
            out_file.close
        end

    end
end
