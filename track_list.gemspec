lib = File.expand_path("lib", __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "track_list/version"

Gem::Specification.new do |spec|
  spec.name          = "track_list"
  spec.version       = TrackList::VERSION
  spec.authors       = ["Domenic Fiore"]
  spec.email         = ["domenicfiore@gmail.com"]

  spec.summary       = %q{Get the tracklist of a directory.}
  spec.description   = %q{Output the tracklist of an album from a directory.}
  spec.homepage      = "https://gitlab.com/DomenicF/track_list"
  spec.license       = "MIT"

  spec.metadata["allowed_push_host"] = "https://rubygems.org"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/DomenicF/track_list"
  #spec.metadata["changelog_uri"] = "TODO: Put your gem's CHANGELOG.md URL here."

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "bin"
  spec.executables   = ["tracklist"]
  spec.require_paths = ["lib"]

  spec.add_runtime_dependency "taglib-ruby", "~> 1.1"
  spec.add_development_dependency "bundler", ">= 2.2"
  spec.add_development_dependency "rake", "~> 13.0"
end
